<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
		<script src="https://cdn.staticfile.org/vue/2.2.2/vue.min.js"></script>
		<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">  
		<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/new_file.css"/>
	</head>
	<body>
		<div id="zhong" >
			<div id="title">
				<h3>考试信息</h3>
			</div>
			<div id="top">
				<div id="topleft" style="width: 400px;margin-left: 25px;float: left;">
					<form class="bs-example bs-example-form" role="form" style="width: 800px;float: left;" method = "post">
					        <div class="input-group input-group-lg"  style="float: left;" >
					            <input type="text" class="form-control" placeholder="搜索"  name="ppp">
					        </div>
					
					<div id="topright" class="text-right" style="float: left;">
						<input type="submit" class="btn btn-primary btn-sm "style="height: 46px;" name="put">
					</div>
					</form>
				</div>
			</div>
			<div id="biaoge">
				<table class="table table-bordered " id="app">
				  <thead>
				    <tr style="background-color: aquamarine;font-weight: bold;font-size: 15px;">
				      <th class="col-lg-1">考试班级</th>
				      <th class="col-lg-1">考试科目</th>
					  <th class="col-lg-2">考试时间</th>
				      <th class="col-lg-3">考试地点</th>
					  <th class="col-lg-3">监考老师</th>
				    </tr>
				  </thead>
				  <?php
				  include_once("conn.php");
				  $pagesize = 2 ;									//每页显示记录数
				  $sqlstr = "select * from text where time!='' ";//定义查询语句
				  $total = mysqli_query($conn,$sqlstr);//执行查询语句
				  $totalNum = mysqli_num_rows($total);					//总记录数
				  $pagecount = ceil($totalNum/$pagesize);						//总页数
				  (!isset($_GET['page']))?($page = 1):$page = $_GET['page'];				//当前显示页数
				  ($page <= $pagecount)?$page:($page = $pagecount);//当前页大于总页数时把当前页定义为总页数
				  $f_pageNum = $pagesize * ($page - 1);								//当前页的第一条记录
				  if(isset($_POST['put'])){
				  $text=$_POST['ppp'];
				  $text=strval($text);
				   $sqlstr1="select * from text where (class LIKE '%$text%' or kemu LIKE '$text%' or didian LIKE '$text%' or teather LIKE '$text%')and (time!='' and didian!='' and teather!='') ";}//执行查询创建结果集
				  else{$sqlstr1 = $sqlstr." limit ".$f_pageNum.",".$pagesize;}//执行查询创建结果集}
				  
				  $result = mysqli_query($conn,$sqlstr1);
				  ?>
				  <tbody>
					  <?php
					  while($row = mysqli_fetch_array($result)){   //读取一条记录到数组$row
					  ?>
				    <tr>
				      <td><?php echo $row[0]?></td>
				      <td><?php echo $row[1]?></td>
				      <td><?php echo $row[2]?></td>
					  <td><?php echo $row[3]?></td>
					  <td><?php echo $row[4]?></td>
				    </tr>
					<?php
					}
					
					?>
					<tr>
						<td  height="25" colspan="6" align="left">
					<?php
					    echo "共".$totalNum."场考试&nbsp;&nbsp;";
						echo "第".$page."页/共".$pagecount."页&nbsp;&nbsp;";
						if($page!=1){//如果当前页不是1则输出有链接的首页和上一页
						    echo "<a href='?page=1'>首页</a>&nbsp;";
							echo "<a href='?page=".($page-1)."'>上一页</a>&nbsp;&nbsp;";
						}else{//否则输出没有链接的首页和上一页
						    echo "首页&nbsp;上一页&nbsp;&nbsp;";
						}
						if($page!=$pagecount){//如果当前页不是最后一页则输出有链接的下一页和尾页
						    echo "<a href='?page=".($page+1)."'>下一页</a>&nbsp;";
							echo "<a href='?page=".$pagecount."'>尾页</a>&nbsp;&nbsp;";
						}else{//否则输出没有链接的下一页和尾页
						    echo "下一页&nbsp;尾页&nbsp;&nbsp;";
						}
					?></td>
						</tr>
				  </tbody>
				</table>
			</div>
			</div>
		</div>
	</body>
</html>