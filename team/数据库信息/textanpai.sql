/*
Navicat MySQL Data Transfer

Source Server         : php
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : textanpai

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-12 17:12:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for text
-- ----------------------------
DROP TABLE IF EXISTS `text`;
CREATE TABLE `text` (
  `class` varchar(255) DEFAULT NULL,
  `kemu` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT '',
  `didian` varchar(255) DEFAULT NULL,
  `teather` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of text
-- ----------------------------
INSERT INTO `text` VALUES ('软工1', '软件工程', '2020-11-12 09:00', '化工304', '亓');
INSERT INTO `text` VALUES ('软工1', '前端应用框架', '2020-11-12 09:00', '化工312', '安倍平平');
INSERT INTO `text` VALUES ('软工2', '人机交互', '2020-11-12 09:00', '化工317', '娟');
INSERT INTO `text` VALUES ('软工2', '数据库', '2020-11-12 09:00', '化工316', '翁');
INSERT INTO `text` VALUES ('软工1', 'PHP', '2020-11-12 09:00', '轻工612', '杨');
INSERT INTO `text` VALUES ('软工1班', 'uml', '2020-12-13 19：00', '轻工213', '亓');

-- ----------------------------
-- Table structure for useu
-- ----------------------------
DROP TABLE IF EXISTS `useu`;
CREATE TABLE `useu` (
  `user` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `number` varchar(32) DEFAULT NULL,
  `stu_or_ter` varchar(2) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of useu
-- ----------------------------
INSERT INTO `useu` VALUES ('stu1', '123456', '软工1', '211806306', '0');
INSERT INTO `useu` VALUES ('ter', '123456', '教师', '211806344', '1');
INSERT INTO `useu` VALUES ('csl', '123456', '123456', '11111111', '0');
INSERT INTO `useu` VALUES ('stu2', '11111111', '软工1', '11111111', '0');
